(define (problem wsc)
	(:domain
		wsc
	)

	(:objects
		wolf
		sheep
		cabbage
		man
		side1
		side2
	)

	(:init
		(man man)(wolf wolf)(sheep sheep)(cabbage cabbage) (side side1) (side side2)
		(at man side1) (at wolf side1) (at cabbage side1) (at sheep side1)
	)

	(:goal
		(and
			(at man side2) (at wolf side2) (at cabbage side2) (at sheep side2)
		)
	)
)