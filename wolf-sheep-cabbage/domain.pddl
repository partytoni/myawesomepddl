(define
	(domain wsc)

	(:predicates
		(man ?man)
		(wolf ?wolf)
		(sheep ?sheep)
		(cabbage ?cabbage)
		(side ?side)
		(at ?agent ?side)
	)

	(:action carry-wolf
		:parameters(
			?man
			?wolf
			?sheep
			?cabbage
			?from-side
			?to-side
		)

		:precondition(
			and (man ?man)
			(wolf ?wolf)
			(sheep ?sheep)
			(cabbage ?cabbage)

			(at ?man ?from-side)
			(at ?wolf ?from-side)
			(not(at ?man ?to-side))
			(not(at ?wolf ?to-side))
			(not(and(at ?sheep ?from-side) 
				(at ?cabbage ?from-side))
			)
		)

		:effect(
			and 
			(not(at ?man ?from-side))
			(not(at ?wolf ?from-side))
			(at ?man ?to-side)
			(at ?wolf ?to-side)
		)
	)

	(:action carry-sheep
		:parameters(
			?man
			?wolf
			?sheep
			?cabbage
			?from-side
			?to-side
		)

		:precondition(
			and (man ?man)
			(wolf ?wolf)
			(sheep ?sheep)
			(cabbage ?cabbage)

			(at ?man ?from-side)
			(at ?sheep ?from-side)
			(not(at ?man ?to-side))
			(not(at ?sheep ?to-side))
		)

		:effect(
			and 
			(not(at ?man ?from-side))
			(not(at ?sheep ?from-side))
			(at ?man ?to-side)
			(at ?sheep ?to-side)
		)
	)

	(:action carry-cabbage
		:parameters(
			?man
			?wolf
			?sheep
			?cabbage
			?from-side
			?to-side
		)

		:precondition(
			and (man ?man)
			(wolf ?wolf)
			(sheep ?sheep)
			(cabbage ?cabbage)

			(at ?man ?from-side)
			(at ?cabbage ?from-side)
			(not(at ?man ?to-side))
			(not(at ?cabbage ?to-side))
			(not(and(at ?sheep ?from-side) 
				(at ?wolf ?from-side))
			)
		)

		:effect(
			and 
			(not(at ?man ?from-side))
			(not(at ?cabbage ?from-side))
			(at ?man ?to-side)
			(at ?cabbage ?to-side)
		)
	)

	(:action carry-nothing
		:parameters(
			?man
			?wolf
			?sheep
			?cabbage
			?from-side
			?to-side
		)

		:precondition(
			and (man ?man)
			(wolf ?wolf)
			(sheep ?sheep)
			(cabbage ?cabbage)

			(at ?man ?from-side)
			(not(at ?man ?to-side))
			(not(and(at ?sheep ?from-side) 
				(at ?cabbage ?from-side))
			)

			(not(and(at ?sheep ?from-side) 
				(at ?wolf ?from-side))
			)
		)

		:effect(
			and 
			(not(at ?man ?from-side))
			(at ?man ?to-side)
		)
	)
)