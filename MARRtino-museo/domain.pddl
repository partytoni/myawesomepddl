(define
	(domain marrtino-domain)
	(:predicates
		(agent ?a)
		(pos ?p)
		(switch ?l)
		(switchon ?switch)
		(people ?p)
		(at ?x ?pos)
		(wet ?pos)
		(played ?agent ?people)
		(adjacent ?p1 ?p2)
	)

	(:action move
		:parameters(
			?agent
			?from
			?to
		)

		:precondition(
			and(at ?agent ?from)
			(not(at ?agent ?to))
			(not(wet ?to))
			(agent ?agent)
			(pos ?from)
			(pos ?to)
			(adjacent ?from ?to)
		)

		:effect(
			and(at ?agent ?to)
			(not(at ?agent ?from))
		)
	)

	(:action turnlight
		:parameters(
			?switch
			?agent
			?pos
		)

		:precondition(
			and(switch ?switch)
			(agent ?agent)
			(pos ?pos)
			(at ?agent ?pos)
			(at ?switch ?pos)
			(not(switchon ?switch))
		)

		:effect(
			and(switchon ?switch)
		)
	)

	(:action play
		:parameters(
			?agent
			?people
			?pos
			?switch
		)

		:precondition(
			and(agent ?agent)
			(people ?people)
			(pos ?pos)
			(at ?agent ?pos)
			(at ?people ?pos)
			(switchon ?switch)
			(not(played ?agent ?people))
		)

		:effect(
			and(played ?agent ?people)
		)
	)
)