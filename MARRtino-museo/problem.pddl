(define (problem marrtino-problem)
	(:domain marrtino-domain)

	(:objects
		agent
		pos1 pos2 pos3 pos4
		pos5 pos6 pos7 pos8
		people1 people3 people7 switch6
	)

	(:init
		(agent agent) 
		(pos pos1) (pos pos2) (pos pos3) (pos pos4)
		(pos pos5) (pos pos6) (pos pos7) (pos pos8)
		(people people1) (people people3) (people people7)
		(wet pos2) (switch switch6)

		(adjacent pos1 pos2) (adjacent pos2 pos1)
		(adjacent pos2 pos3) (adjacent pos3 pos2) 
		(adjacent pos3 pos4) (adjacent pos4 pos3) 
		(adjacent pos5 pos6) (adjacent pos6 pos5)
		(adjacent pos6 pos7) (adjacent pos7 pos6)    
		(adjacent pos7 pos8) (adjacent pos8 pos7)    
		(adjacent pos1 pos5) (adjacent pos5 pos1)    
		(adjacent pos2 pos6) (adjacent pos6 pos2)    
		(adjacent pos3 pos7) (adjacent pos7 pos3)    
		(adjacent pos4 pos8) (adjacent pos8 pos4)

		(at agent pos5) (at switch6 pos6) (at people1 pos1)
		(at people3 pos3) (at people7 pos7) 	   
	)

	(:goal
		(and
			(switchon switch6) (played agent people1) (played agent people3) (played agent people7)
		)
	)
)