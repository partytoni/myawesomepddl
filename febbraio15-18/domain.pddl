(define 
	(domain febbraio)
	(:predicates 
		(fridge ?fridge)
		(screw ?s)
		(compressor ?c)
		(on ?cose ?fridge); cose può essere compressor o screw
		(fridgepoweron ?fridge)
	)

	(:action turnoff
		:parameters(
			?fridge
			?armscrew
		)

		:precondition(
			and(fridge ?fridge)
			(fridgepoweron ?fridge)
		)

		:effect(
			and(not(fridgepoweron ?fridge))
		)
	)

	(:action removescrew
		:parameters(
			?screw
			?fridge
		)

		:precondition(
			and(screw ?screw)
			(fridge ?fridge)
			(on ?screw ?fridge)
			(not(fridgepoweron ?fridge))
		)

		:effect(
			and(not(on ?screw ?fridge))
		)
	)

	(:action mountscrew
		:parameters(
			?screw
			?fridge
		)

		:precondition(
			and(screw ?screw)
			(fridge ?fridge)
			(not(on ?screw ?fridge))
			(not(fridgepoweron ?fridge))
		)

		:effect(
			and(on ?screw ?fridge)
		)
	)

	(:action removecompressor
		:parameters(
			?compressor
			?fridge
			?screw1
			?screw2
		)

		:precondition(
			and(compressor ?compressor)
			(fridge ?fridge)
			(screw ?screw1)
			(screw ?screw2)
			(not(on ?screw1 ?fridge))
			(not(on ?screw2 ?fridge))
			(on ?compressor ?fridge)
			(not(fridgepoweron ?fridge))

		)

		:effect(
			and(not(on ?compressor ?fridge))
		)
	)

	(:action mountcompressor
		:parameters(
			?compressor
			?fridge
			?screw1
			?screw2
		)

		:precondition(
			and(compressor ?compressor)
			(fridge ?fridge)
			(screw ?screw1)
			(screw ?screw2)
			(not(on ?screw1 ?fridge))
			(not(on ?screw2 ?fridge))
			(not(on ?compressor ?fridge))
			(not(fridgepoweron ?fridge))

		)

		:effect(
			and(on ?compressor ?fridge)
		)
	)
)