(define (problem febbraio-prob)
	(:domain febbraio)

	(:objects
		fridge
		compressorbroken compressornew
		screw1 screw2
	)

	(:init
		(fridge fridge)
		(compressor compressorbroken)
		(compressor compressornew)
		(screw screw1)
		(screw screw2)
		(fridgepoweron fridge)
		(on screw1 fridge)
		(on screw2 fridge)
		(on compressorbroken fridge)
		
	)

	(:goal
		(and
			
			(on screw1 fridge)
			(on screw2 fridge)
			(on compressornew fridge)
			
		)
	)
)