(define 
	(domain one-arm-domain)
	
	(:predicates 
		(at ?X ?Y)
		(clear ?X)
		(pos ?X)
		(cup ?X)
	)
	(:action move
		:parameters(
			?FROM 
			?TO
			?CUP
		)
		:effect (
			and
				(at ?CUP ?TO)
				(not (at ?CUP ?FROM))
				(clear ?FROM)
				(not (clear ?TO))
			
		)
		:precondition (
			and
				(cup ?CUP)
				(pos ?FROM)
				(pos ?TO)
				(at ?CUP ?FROM)
				(not (at ?CUP ?TO))
				(not (clear ?FROM))
				(clear ?TO)
			
		)
	)
)
