(define (problem one-arm)
	(:domain 
		one-arm-domain
	)
	(:objects
		yellow
		red
		blue
		pos1
		pos2
		pos3
		pos4
	)
	(:init 
		(cup yellow) (cup red) (cup blue) (pos pos1)
		(pos pos2) (pos pos3) (pos pos4) (at yellow pos1) 
		(at red pos3) (at blue pos4) (not(clear pos1)) 
		(not(clear pos3)) (not(clear pos4)) (clear pos2)
	)

	(:goal
		(and
			(at red pos1)
			(at yellow pos2)
			(at blue pos3) 
			(not(clear pos1))
			(not(clear pos2))
			(not(clear pos3))
			(clear pos4)
		)
	)
)
