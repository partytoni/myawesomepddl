(define (problem monbana)
	(:domain monkeybanana)

	(:objects
		monkey
		banana
		box
		pos1
		pos2
		pos3
	)

	(:init 
		(monkey monkey) (banana banana) (box box) (pos pos1) (pos pos2) (pos pos3) 
		(at monkey pos1) (at banana pos2) (at box pos3) 
	)

	(:goal
		(and (hasbanana monkey banana))	
	)
)