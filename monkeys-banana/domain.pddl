(define (domain monkeybanana)
	(:predicates
		(monkey ?m)
		(banana ?b)
		(box ?b)
		(pos ?p)
		(at ?x ?pos)	
		(on ?m ?b)
		(hasbanana ?m ?b)
	)

	(:action go
		:parameters(
			?monkey
			?from
			?to
			?box
		)

		:precondition(
			and(monkey ?monkey)
			(pos ?from)
			(pos ?to)
			(at ?monkey ?from)
			(not(at ?monkey ?to))
			(not(on ?monkey ?box))
		)

		:effect(
			and(at ?monkey ?to)
			(not(at ?monkey ?from))
		)
	)

	(:action grab
		:parameters(
			?monkey
			?banana
			?box
			?pos
		)

		:precondition(
			and (monkey ?monkey)
			(banana ?banana)
			(box ?box)
			(pos ?pos)
			(at ?monkey ?pos)
			(at ?banana ?pos)
			(at ?box ?pos)
			(on ?monkey ?box)
			(not(hasbanana ?monkey ?banana))
		)

		:effect(
			and(hasbanana ?monkey ?banana)
		)
	)

	(:action push
		:parameters(
			?monkey
			?box
			?frompos
			?topos
		)

		:precondition(
			and (monkey ?monkey)
			(box ?box)
			(pos ?frompos)
			(pos ?topos)
			(at ?monkey ?frompos)
			(at ?box ?frompos)
			(not(at ?monkey ?topos))
			(not(at ?box ?topos))
		)

		:effect(
			and(at ?monkey ?topos)
			(at ?box ?topos)
			(not(at ?monkey ?frompos))
			(not(at ?box ?frompos))
		)
	)

	(:action climb
		:parameters(
			?monkey
			?box
			?pos
		)

		:precondition(
			and (monkey ?monkey)
			(box ?box)
			(pos ?pos)
			(at ?monkey ?pos)
			(at ?box ?pos)
			(not(on ?monkey ?box))
		)

		:effect(
			and(on ?monkey ?box)
		)
	)
)