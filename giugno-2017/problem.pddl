(define 
	(problem robocup-problem)
	(:domain robocup-domain)

	(:objects
		agent product
		pos1 pos2 pos3 pos4 pos5
		basestation ringstation capstation deliverystation
		base ring1 ring2 cap
	)

	(:init
		(agent agent) (product product)

		(pos pos1) (pos pos2) (pos pos3) (pos pos4) (pos pos5)

		(basestation basestation) (ringstation ringstation) (capstation capstation) (deliverystation deliverystation)

		(base base) (ring ring1) (ring ring2) (cap cap) 

		(at agent pos1) (at basestation pos2) (at ringstation pos3) (at capstation pos4) (at deliverystation pos5)
	)

	(:goal
		(and(hasbase product) (hasring product ring1) (hasring product ring2) (hascap product) (delivered product))
	)
)