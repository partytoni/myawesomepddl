(define 
	(domain robocup-domain)

	(:predicates
		(agent ?agent)
		(product ?product)
		(basestation ?basestation)
		(ringstation ?ringstation)
		(capstation ?capstation)
		(deliverystation ?deliverystation)
		(pos ?pos)
		(base ?base)
		(ring ?ring)
		(cap ?cap)
		(hasbase ?product)
		(hasring ?product ?ring)
		(hascap ?product)
		(delivered ?product)
		(at ?x ?pos)
	)

	(:action move
		:parameters(
			?from
			?to
			?agent
		)

		:precondition(
			and(agent ?agent)
			(pos ?from)
			(pos ?to)
			(at ?agent ?from)
			(not(at ?agent ?to))
		)

		:effect(
			and(at ?agent ?to)
			(not(at ?agent ?from))
		)
	)

	(:action takebase
		:parameters(
			?agent
			?pos
			?basestation
			?product
		)

		:precondition(
			and(agent ?agent)
			(pos ?pos)
			(basestation ?basestation)
			(product ?product)

			(at ?agent ?pos)
			(at ?basestation ?pos)
			(not(hasbase ?product))
		)

		:effect(
			and(hasbase ?product)
		)
	)

	(:action takering
		:parameters(
			?agent
			?pos
			?ringstation
			?product
			?ring
		)

		:precondition(
			and(agent ?agent)
			(pos ?pos)
			(ringstation ?ringstation)
			(product ?product)

			(at ?agent ?pos)
			(at ?ringstation ?pos)
			(hasbase ?product)
			(not(hascap ?product))
		)

		:effect(
			and(hasring ?product ?ring)
		)
	)

	(:action takecap
		:parameters(
			?agent
			?pos
			?capstation
			?product
		)

		:precondition(
			and(agent ?agent)
			(pos ?pos)
			(capstation ?capstation)
			(product ?product)

			(at ?agent ?pos)
			(at ?capstation ?pos)
			(hasbase ?product)
			(not(hascap ?product))
		)

		:effect(
			and(hascap ?product)
		)
	)

	(:action deliver
		:parameters(
			?agent
			?pos
			?deliverystation
			?product
		)

		:precondition(
			and(agent ?agent)
			(pos ?pos)
			(deliverystation ?deliverystation)
			(product ?product)

			(at ?agent ?pos)
			(at ?deliverystation ?pos)
			(hasbase ?product)
			(hascap ?product)
		)

		:effect(
			and(delivered ?product)
		)
	)


)