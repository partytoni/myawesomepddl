(define 
	(domain manipulator-domain)

	(:predicates
		(workingarea ?w)
		(hand ?h)
		(freehand ?h)
		(case ?c)
		(caselocked ?c)
		(key ?k)
		(belong ?k ?c)
		(object ?o)
		(in ?o ?x); x is both hand and case
	)

	(:action graspworkingarea
		:parameters(
			?hand
			?object
			?workingarea
		)

		:precondition(
			and(hand ?hand)
			(freehand ?hand)
			(workingarea ?workingarea)
			(in ?object ?workingarea)
		)

		:effect(
			and(not(freehand ?hand))
			(in ?object ?hand)
			(not(in ?object ?workingarea))
		)
	)	

	(:action graspcase
		:parameters(
			?hand
			?object
			?case
		)

		:precondition(
			and(hand ?hand)
			(freehand ?hand)
			(case ?case)
			(in ?object ?case)
			(not(caselocked ?case))
		)

		:effect(
			and(not(freehand ?hand))
			(in ?object ?hand)
			(not(in ?object ?case))
		)
	)	

	(:action releaseworkingarea
		:parameters(
			?hand
			?object
			?workingarea
		)

		:precondition(
			and(hand ?hand)
			(workingarea ?workingarea)
			(in ?object ?hand)
			(not(freehand ?hand))
		)

		:effect(
			and(freehand ?hand)
			(in ?object ?workingarea)
			(not(in ?object ?hand))
		)
	)	

	(:action releasecase
		:parameters(
			?hand
			?object
			?case
		)

		:precondition(
			and(hand ?hand)
			(case ?case)
			(in ?object ?hand)
			(not(freehand ?hand))
			(not(caselocked ?case))
		)

		:effect(
			and(freehand ?hand)
			(in ?object ?case)
			(not(in ?object ?hand))
		)
	)	

	(:action lock
		:parameters(
			?hand
			?case
			?key
		)

		:precondition(
			
			and(not(freehand ?hand))
			(in ?key ?hand)
			(not(caselocked ?case))
			(belong ?key ?case)
			(hand ?hand)
			(case ?case)
			(key ?key)
		)

		:effect(
			and(caselocked ?case)
		)
	)

	(:action unlock
		:parameters(
			?hand
			?case
			?key
		)

		:precondition(
			
			and(not(freehand ?hand))
			(in ?key ?hand)
			(caselocked ?case)
			(belong ?key ?case)
			(hand ?hand)
			(case ?case)
			(key ?key)
		)

		:effect(
			and(not(caselocked ?case))
		)
	)
)