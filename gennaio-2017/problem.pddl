(define (problem manipulator-problem)
	(:domain
		manipulator-domain
	)

	(:objects
		hand
		object
		case1 
		case2
		key1
		key2
		workingarea
	)

	(:init
		(hand hand) (object object)
		(case case1) (case case2)
		(key key1) (key key2) 
		(workingarea workingarea) (freehand hand)
		(in object case2) (in key1 workingarea) (in key2 workingarea)
		(caselocked case1) (belong key1 case1) (belong key2 case2)
	)

	(:goal
		(and
			(in object case1)
			(in key1 case2)
			(caselocked case1)
			(caselocked case2)
		)
	)
)