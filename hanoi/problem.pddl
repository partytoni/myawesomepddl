(define (problem hanoi-prob)
	(:domain
		hanoi
	)

	(:objects
		disk1 disk2 disk3
		peg1 peg2 peg3
	)

	(:init
		(disk disk1)
		(disk disk2)
		(disk disk3)

		(at disk1 disk2)
		(at disk2 disk3)
		(at disk3 peg1)
		(smaller disk1 disk2)
		(smaller disk1 disk3)
		(smaller disk2 disk3)
		
		(smaller disk1 peg1)
		(smaller disk1 peg2)
		(smaller disk1 peg3)
		
		(smaller disk2 peg1)
		(smaller disk2 peg2)
		(smaller disk2 peg3)
		
		(smaller disk3 peg1)
		(smaller disk3 peg2)
		(smaller disk3 peg3)

		(clear peg2) (clear peg3)
		(clear disk1)
	)

	(:goal
		(and (at disk3 peg3)
		(at disk2 disk3)
		(at disk1 disk2))
	)
)