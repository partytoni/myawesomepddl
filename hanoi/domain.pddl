(define 
	(domain hanoi)
	
	(:predicates 
		(at ?disk ?pos); the ?disk is at position ?pos
		(disk ?d)
		(pos ?p)
		(clear ?b) 
		(smaller ?x ?y); x is smaller than y
	)
	
	(:action move
		:parameters(
			?disk
			?from
			?to
		)

		:precondition(
			and(disk ?disk)
			(smaller ?disk ?to)
			(at ?disk ?from)
			(clear ?to)
			(clear ?disk)
		)

		:effect(
			and(at ?disk ?to)
			(not(at ?disk ?from))
			(clear ?from)
			(not(clear ?to))
		)
	)
)
