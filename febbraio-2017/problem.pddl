(define (problem agentprob)
	(:domain agent)
	(:objects
		agent
		pos1 pos2 pos3
		pos4 pos5 pos6
		pos7 pos8 pos9
		nw nn ne
		ww    ee
		sw ss se
	)
	(:init
		(agent agent)
		(pos pos1) (pos pos2) (pos pos3)
		(pos pos4) (pos pos5) (pos pos6)
		(pos pos7) (pos pos8) (pos pos9)
		(orientation nw) (orientation nn) (orientation ne)
		(orientation ww)                  (orientation ee)
		(orientation sw) (orientation ss) (orientation se)
		(at agent pos7) (at agent ne)
		(busy pos5) (busy pos1)

		(canmove agent pos1 ee pos2)
		(canmove agent pos1 se pos5)
		(canmove agent pos1 ss pos4)

		(canmove agent pos2 ee pos3)
		(canmove agent pos2 se pos6)
		(canmove agent pos2 ss pos5)
		(canmove agent pos2 sw pos4)
		(canmove agent pos2 ww pos1)

		(canmove agent pos3 ss pos6)
		(canmove agent pos3 sw pos5)
		(canmove agent pos3 ww pos2)

		(canmove agent pos4 nn pos1)
		(canmove agent pos4 ne pos2)
		(canmove agent pos4 ee pos5)
		(canmove agent pos4 se pos8)
		(canmove agent pos4 ss pos7)

		(canmove agent pos5 nn pos2)
		(canmove agent pos5 ne pos3)
		(canmove agent pos5 ee pos6)
		(canmove agent pos5 se pos9)
		(canmove agent pos5 ss pos8)
		(canmove agent pos5 sw pos7)
		(canmove agent pos5 ww pos4)
		(canmove agent pos5 nw pos1)

		(canmove agent pos6 nn pos3)
		(canmove agent pos6 nw pos2)
		(canmove agent pos6 ww pos5)
		(canmove agent pos6 sw pos8)
		(canmove agent pos6 ss pos9)

		(canmove agent pos7 nn pos4)
		(canmove agent pos7 ne pos5)
		(canmove agent pos7 ee pos8)

		(canmove agent pos8 ww pos7)
		(canmove agent pos8 nw pos4)
		(canmove agent pos8 nn pos5)
		(canmove agent pos8 ne pos6)
		(canmove agent pos8 ee pos9)

		(canmove agent pos9 nn pos6)
		(canmove agent pos9 nw pos5)
		(canmove agent pos9 ww pos8)


		(canori nw sw)
		(canori nw ww)
		(canori nw nn)
		(canori nw ne)

		(canori nn ww)
		(canori nn nw)
		(canori nn ne)
		(canori nn ee)

		(canori ne ww)
		(canori ne nn)
		(canori ne ee)
		(canori ne se)

		(canori ww nn)
		(canori ww nw)
		(canori ww sw)
		(canori ww ss)

		(canori ee nn)
		(canori ee ne)
		(canori ee ss)
		(canori ee se)

		(canori sw nw)
		(canori sw ww)
		(canori sw ss)
		(canori sw se)

		(canori ss nw)
		(canori ss ww)
		(canori ss ee)
		(canori ss se)

		(canori se sw)
		(canori se ss)
		(canori se ee)
		(canori se ne)

	)
	(:goal
		(and 
			(at agent pos9)
			(at agent nn)
		)
	)
)
