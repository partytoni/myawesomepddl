(define
	(domain agent)
	(:predicates
		(agent ?agent)
		(pos ?pos)
		(at ?x ?y); x can be agent or obstacle, y can be pos or orientation
		(busy ?pos)
		(orientation ?o)
		(hasChangedOrientation ?agent)
		(canmove ?agent ?frompos ?orientation ?topos)
		(canori ?fromori ?toori)

	)
	(:action move
		:parameters(
			?agent
			?frompos
			?topos
			?orientation
		)
		:precondition(
			and
			(agent ?agent)
			(pos ?frompos)
			(pos ?topos)
			(orientation ?orientation)
			(at ?agent ?frompos)
			(at ?agent ?orientation)
			(not(at ?agent ?topos))
			(not(busy ?topos))
			(canmove ?agent ?frompos ?orientation ?topos)
		)
		:effect(
			and
			(at ?agent ?topos)
			(not(at ?agent ?frompos))
			(busy ?topos)
			(not(busy ?frompos))
			(not(hasChangedOrientation ?agent))
		)
	)
	(:action changeorientation
		:parameters(
			?agent
			?orientationfrom
			?orientationto
			?pos
		)
		:precondition(
			and
			(agent ?agent)
			(pos ?pos)
			(orientation ?orientationfrom)
			(orientation ?orientationto)
			(not(hasChangedOrientation ?agent))
			(at ?agent ?pos)
			(at ?agent ?orientationfrom)
			(not(at ?agent ?orientationto))
			(canori ?orientationfrom  ?orientationto)
		)
		:effect(
			and
			(at ?agent ?orientationto)
			(not(at ?agent ?orientationto))
			(hasChangedOrientation ?agent)
		)
	)
)
