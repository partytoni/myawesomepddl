(define 
	(domain marrtino)
	
	(:predicates 
		(robot ?robot)
		(pos ?pos)
		(dirt ?dirt)
		(people ?p)
		(at ?robot ?pos)
		(atdirt ?dirt ?pos)
		(atpeople ?people ?pos)
		(adjacent ?pos1 ?pos2)
	)
	
	(:action move
		:parameters(
			?robot
			?from
			?to ; from and to can be rooms or pos
			?people
		)

		:precondition(
			and(robot ?robot)
			(pos ?from)
			(pos ?to)
			(people ?people)
			(adjacent ?from ?to)
			(at ?robot ?from)
			(not(at ?robot ?to))
			(not(atpeople ?people ?to))
		)

		:effect(
			and(at ?robot ?to)
			(not(at ?robot ?from))
		)
	)

	(:action clean
		:parameters(
			?robot
			?pos
			?dirt
		)

		:precondition(
			and(robot ?robot)
			(pos ?pos)
			(atdirt ?dirt ?pos)
			(at ?robot ?pos)
		)

		:effect(
			and(not(atdirt ?dirt ?pos))
		)
	)
)