(define (problem marrtino-prob)
	(:domain marrtino)

	(:objects
		pos1 pos2 pos3
		pos4 pos5 pos6
		room1 room2 room3
		room4 room5 room6
		robot dirt people
	)

	(:init
		(pos pos1) (pos pos2) (pos pos3)
		(pos pos4) (pos pos5) (pos pos6)
		(pos room1) (pos room2) (pos room3)
		(pos room4) (pos room5) (pos room6)

		(dirt dirt) (people people)

		(robot robot) (at robot pos1)

		(adjacent pos1 room1) (adjacent pos2 room2) (adjacent pos3 room3) 
		(adjacent pos4 room4) (adjacent pos5 room5) (adjacent pos6 room6) 
		(adjacent pos1 pos2) (adjacent pos1 pos4) (adjacent pos2 pos5) 
		(adjacent pos2 pos3) (adjacent pos3 pos6) (adjacent pos4 pos5) 
		(adjacent pos5 pos6)

		(adjacent room1 pos1) (adjacent room2 pos2) (adjacent room3 pos3) 
		(adjacent room4 pos4) (adjacent room5 pos5) (adjacent room6 pos6) 
		(adjacent pos2 pos1) (adjacent pos4 pos1) (adjacent pos5 pos2) 
		(adjacent pos3 pos2) (adjacent pos6 pos3) (adjacent pos5 pos4) 
		(adjacent pos6 pos5)

		(atpeople people room1) (atpeople people room3) (atpeople people room4) (atpeople people room5)

		(atdirt dirt room2) (atdirt dirt room4) (atdirt dirt room6)
	)

	(:goal 
		(and
			(not(atdirt dirt room2))
			(not(atdirt dirt room6))
		)
	)
)